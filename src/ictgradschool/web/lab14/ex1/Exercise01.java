package ictgradschool.web.lab14.ex1;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }



        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful");
            while (true) {
                System.out.print("Please enter article name: ");
                Scanner scanner = new Scanner(System.in);
                String articleName = scanner.nextLine();
                String articleContent = null;
                try (PreparedStatement selection = conn.prepareStatement("SELECT body FROM simpledao_articles WHERE title = ?")) {
                    selection.setString(1, articleName);

                    try (ResultSet resultSet = selection.executeQuery()) {
                        while (resultSet.next()) {
                            articleContent = resultSet.getNString(1);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    if(articleContent!=null){
                        System.out.println(articleContent);
                        break;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
