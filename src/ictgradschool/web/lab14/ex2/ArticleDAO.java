package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class ArticleDAO {
    public List<Article> getAllArticles(){
        List<Article> articleList = new ArrayList<>();
        // Set the database name to your database
            /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful");
                try (Statement selection = conn.createStatement()){
                    selection.executeQuery("SELECT * FROM simpledao_articles");
                    try (ResultSet resultSet = selection.getResultSet()) {
                        while (resultSet.next()) {
                            articleList.add(new Article(resultSet.getString(resultSet.findColumn("title")),resultSet.getString(resultSet.findColumn("body"))));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articleList;
    }
}
