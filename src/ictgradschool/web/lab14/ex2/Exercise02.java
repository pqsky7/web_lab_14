package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.List;
import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        List<Article> allArticles = new ArticleDAO().getAllArticles();
        while (true) {
            System.out.print("Please enter article name: ");
            Scanner scanner = new Scanner(System.in);
            String articleTitle = scanner.nextLine();
            for (Article article : allArticles) {
                if (article.getTitle().equals(articleTitle)) {
                    System.out.println(article.getContent());
                    break;
                }
            }
        }
    }
}
