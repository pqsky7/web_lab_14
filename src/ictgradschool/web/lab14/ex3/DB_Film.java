package ictgradschool.web.lab14.ex3;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_Film {
    private String title;
    private String genre;
    private String certificate;

    public DB_Film(String title, String genre, String certificate) {
        this.title = title;
        this.genre = genre;
        this.certificate = certificate;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }
}
