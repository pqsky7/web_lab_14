package ictgradschool.web.lab14.ex3;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_Relationship_Actor_Films {
    private String firstName;
    private String lastName;
    private String filmName;
    private String role;

    public DB_Relationship_Actor_Films(String firstName, String lastName, String filmName, String role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.filmName = filmName;
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFilmName() {
        return filmName;
    }

    public String getRole() {
        return role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

