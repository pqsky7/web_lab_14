package ictgradschool.web.lab14.ex3;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_Actor {
    String firstName;
    String lastName;

    public DB_Actor(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
