DROP TABLE IF EXISTS db1ab14_ex03_act;
DROP TABLE IF EXISTS db1ab14_ex03_have;
DROP TABLE IF EXISTS db1ab14_ex03_film;
DROP TABLE IF EXISTS db1ab14_ex03_actor;
DROP TABLE IF EXISTS db1ab14_ex03_genres;
DROP TABLE IF EXISTS db1ab14_ex03_producers;
DROP TABLE IF EXISTS db1ab14_ex03_certificates;


CREATE TABLE db1ab14_ex03_certificates (
  certificate VARCHAR(99) NOT NULL,
  PRIMARY KEY (certificate)
);

CREATE TABLE db1ab14_ex03_producers (
  producer VARCHAR(99) NOT NULL,
  PRIMARY KEY (producer)
);

CREATE TABLE db1ab14_ex03_genres (
  genre VARCHAR(99) NOT NULL,
  PRIMARY KEY (genre)
);

CREATE TABLE db1ab14_ex03_actor (
  fname VARCHAR(99) NOT NULL,
  lname VARCHAR(99) NOT NULL,
  PRIMARY KEY (fname,lname)
);

CREATE TABLE db1ab14_ex03_film (
  filmname VARCHAR(99) NOT NULL,
  genre VARCHAR(99) NOT NULL,
  certificate VARCHAR(99) NOT NULL,
  PRIMARY KEY (filmname),
  FOREIGN KEY (genre) REFERENCES db1ab14_ex03_genres (genre),
  FOREIGN KEY (certificate) REFERENCES db1ab14_ex03_certificates (certificate)
);

CREATE TABLE db1ab14_ex03_have (
  producer VARCHAR(99) NOT NULL,
  filmname VARCHAR(99) NOT NULL,
  PRIMARY KEY (filmname),
  FOREIGN KEY (producer) REFERENCES db1ab14_ex03_producers(producer),
  FOREIGN KEY (filmname) REFERENCES db1ab14_ex03_film(filmname)
);

CREATE TABLE db1ab14_ex03_act (
  fname VARCHAR(99) NOT NULL,
  lname VARCHAR(99) NOT NULL,
  filmname VARCHAR(99) NOT NULL,
  role VARCHAR(99) NOT NULL,
  PRIMARY KEY (filmname,fname,lname),
  FOREIGN KEY (fname,lname) REFERENCES db1ab14_ex03_actor(fname,lname),
  FOREIGN KEY (filmname) REFERENCES db1ab14_ex03_film(filmname)
);


INSERT INTO db1ab14_ex03_certificates (certificate) VALUES
  ('PG'),
  ('PG-13'),
  ('R'),
  ('NC-17');

INSERT INTO db1ab14_ex03_producers (producer) VALUES
  ('J. J. Abrams'),
  ('Buddy Adler'),
  ('Judd Apatow'),
  ('Avi Arad'),
  ('Drew Barrymore'),
  ('Lawrence Bender'),
  ('Armyan Bernstein');

INSERT INTO db1ab14_ex03_genres (genre) VALUES
  ('Adventure'),
  ('Mystery'),
  ('Action'),
  ('Comedies'),
  ('Crime'),
  ('Dramas'),
  ('Horror');

INSERT INTO db1ab14_ex03_actor (fname, lname) VALUES
  ('Tom','Cruise'),
  ('Christina ','Applegate'),
  ('Marilyn','Monroe'),
  ('Don','Murray'),
  ('David','Koechner'),
  ('Tobey','Maguire'),
  ('Hugh','Jackman'),
  ('Patrick','Stewart');

INSERT INTO db1ab14_ex03_film (filmname, genre, certificate) VALUES
  ('Mission: Impossible III','Action','R'),
  ('Bus Stop','Comedies','R'),
  ('Anchorman: The Legend of Ron Burgundy','Comedies','PG'),
  ('Spider-Man','Action','PG-13'),
  ('X-Men','Action','R'),
  ('X-Men II','Action','R');

INSERT INTO db1ab14_ex03_have (filmname,producer) VALUES
  ('Mission: Impossible III','J. J. Abrams'),
  ('Bus Stop','Buddy Adler'),
  ('Anchorman: The Legend of Ron Burgundy','Judd Apatow'),
  ('Spider-Man','Avi Arad '),
  ('X-Men','Avi Arad '),
  ('X-Men II','Avi Arad ');

INSERT INTO db1ab14_ex03_act (fname, lname, filmname,role) VALUES
  ('Tom','Cruise','Mission: Impossible III','Spy'),
  ('Christina ','Applegate','Anchorman: The Legend of Ron Burgundy','Man'),
  ('Marilyn','Monroe','Bus Stop','Woman'),
  ('Don','Murray','Bus Stop','Man'),
  ('Tobey','Maguire','Spider-Man','Spider-Man'),
  ('Hugh','Jackman','X-Men','Wolverine'),
  ('Patrick','Stewart','X-Men','Professor-X'),
  ('Hugh','Jackman','X-Men II','Wolverine'),
  ('Patrick','Stewart','X-Men II','Professor-X'),
  ('David','Koechner','Anchorman: The Legend of Ron Burgundy','Man');