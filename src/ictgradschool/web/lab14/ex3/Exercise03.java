package ictgradschool.web.lab14.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exercise03 {

    public DB_MovieInfoDAO_Typical db_movieInfoDAO;
    public List<DB_Actor> actorList;
    public List<DB_Film> filmList;
    public List<DB_Relationship_Actor_Films> RAF_List;
    public List<DB_Relationship_Film_Producer> RFP_List;
    public Scanner scanner;
    public String userInput;

    public void start() {
        /*
        //  DAO: loop loading,multiple loading within one connection and less duplicate code, but Andrew said that this coding is UN-NORMAL.
        DB_MovieInfo movieInfo = new DB_MovieInfoDAO().getMoviesInfo();
        actorList = movieInfo.getActorList();
        filmList = movieInfo.getFilmList();
        RAF_List = movieInfo.getRAF_List();
        RFP_List = movieInfo.getRFP_List();
        */

        ///*
        //  DAO_Typical: single loading with many connection
        db_movieInfoDAO = new DB_MovieInfoDAO_Typical();
        actorList = db_movieInfoDAO.getActorTable();
        filmList = db_movieInfoDAO.getFilmTable();
        RAF_List = db_movieInfoDAO.getRAFTable();
        RFP_List = db_movieInfoDAO.getRFPTable();
        // */

        /*
        //  DAO_TypicalNEO: multiple loading within one connection, but there are many duplicate code
        DB_MovieInfo movieInfo = new DB_MovieInfoDAO_TypicalNEO().getMoviesInfo();
        actorList = movieInfo.getActorList();
        filmList = movieInfo.getFilmList();
        RAF_List = movieInfo.getRAF_List();
        RFP_List = movieInfo.getRFP_List();
        */

        scanner = new Scanner(System.in);
        userInput = null;
        System.out.println();
        mainMenu();
    }

    public void mainMenu() {
        System.out.println("Welcome to the Film database!");
        outLoop:
        while (true) {
            System.out.println("Please select an option from the following:");
            System.out.println("1. Information by Actor");
            System.out.println("2. Information by Movie");
            System.out.println("3. Information by Genre");
            System.out.println("4. Exit");
            System.out.print(">");

            getUserInput();
            switch (userInput) {
                case "1":
                    infoByActor();
                    break;
                case "2":
                    infoByMovie();
                    break;
                case "3":
                    infoByGenre();
                    break;
                case "4":
                    break outLoop;
            }
        }
    }

    public void infoByActor() {
        while (true) {
            ArrayList<String> filmNames = new ArrayList<>();
            ArrayList<String> roles = new ArrayList<>();
            subMenuIntroduction("actor");
            if (userInput.isEmpty()) {
                break;
            }
            for (DB_Relationship_Actor_Films RAF : RAF_List) {
                if (userInput.equals(RAF.getFirstName() + " " + RAF.getLastName())) {
                    filmNames.add(RAF.getFilmName());
                    roles.add(RAF.getRole());
                }
            }

            if (filmNames.isEmpty()) {
                printAlert("actor", "name");
            } else {
                System.out.printf("%s is listed as being involved in the following films:\n", userInput);
                for (int i = 0; i < filmNames.size(); i++) {
                    System.out.printf("%s (%s)\n", filmNames.get(i), roles.get(i));
                }

            }
            System.out.println();
        }
    }

    public void infoByMovie() {

        while (true) {
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> roles = new ArrayList<>();
            String genre = null;
            subMenuIntroduction("film");
            if (userInput.isEmpty()) {
                break;
            }
            for (DB_Relationship_Actor_Films RAF : RAF_List) {
                if (userInput.equals(RAF.getFilmName())) {
                    names.add(RAF.getFirstName() + " " + RAF.getLastName());
                    roles.add(RAF.getRole());
                }
            }

            if (names.isEmpty()) {
                printAlert("film", "title");
            } else {
                for (DB_Film film : filmList) {
                    if (userInput.equals(film.getTitle())) {
                        genre = film.getGenre().toLowerCase();
                    }
                }
                System.out.printf("The film %s is a %s moive that features the following people:\n", userInput, genre);
                for (int i = 0; i < names.size(); i++) {
                    System.out.printf("%s (%s)\n", names.get(i), roles.get(i));
                }
                System.out.println();
            }

        }

    }

    public void infoByGenre() {
        while (true) {
            ArrayList<String> movies = new ArrayList<>();
            subMenuIntroduction("genre");
            if (userInput.isEmpty()) {
                break;
            }
            for (DB_Film film : filmList) {
                if (userInput.equals(film.getGenre())) {
                    movies.add(film.getTitle());
                }
            }

            if (movies.isEmpty()) {
                printAlert("film", "genre");
            } else {
                System.out.printf("The %s genre includes the following films:\n", userInput);
                for (String movie : movies) {
                    System.out.printf("%s\n", movie);
                }
                System.out.println();
            }
        }
    }

    public void getUserInput() {
        userInput = null;
        userInput = scanner.nextLine();
        System.out.println();
    }

    public void subMenuIntroduction(String messageOne) {
        System.out.println("Please enter the name of the " + messageOne + " you wish to get information about, or press enter to return to the previous menu");
        System.out.print(">");
        getUserInput();
    }

    public void printAlert(String messageOne, String messageTwo) {
        System.out.println("Sorry, we couldn't find any " + messageOne + " by that " + messageTwo);
    }

    public static void main(String[] args) {
        new Exercise03().start();
    }

}