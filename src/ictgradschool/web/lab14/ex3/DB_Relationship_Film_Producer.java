package ictgradschool.web.lab14.ex3;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_Relationship_Film_Producer {
    String producer;
    String filmName;

    public DB_Relationship_Film_Producer(String producer, String filmName) {
        this.producer = producer;
        this.filmName = filmName;
    }

    public String getProducer() {
        return producer;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }
}
