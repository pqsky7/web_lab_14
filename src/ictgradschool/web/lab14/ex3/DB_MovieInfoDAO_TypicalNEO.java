package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_MovieInfoDAO_TypicalNEO {
    DB_MovieInfo MovieInfo;
    List<DB_Actor> actorList;
    List<DB_Film> filmList;
    List<DB_Relationship_Actor_Films> RAF_List;
    List<DB_Relationship_Film_Producer> RFP_List;

    public DB_MovieInfoDAO_TypicalNEO() {
        MovieInfo = null;
        actorList = new ArrayList<>();
        filmList = new ArrayList<>();
        RAF_List = new ArrayList<>();
        RFP_List = new ArrayList<>();
    }

    public DB_MovieInfo getMoviesInfo() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful, loading tables...");
            getActorTable(connection);
            getFilmTable(connection);
            getRAFTable(connection);
            getRFPTable(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new DB_MovieInfo(actorList, filmList, RAF_List, RFP_List);
    }

    public List<DB_Actor> getActorTable(Connection connection) throws SQLException {
        try (PreparedStatement selection = connection.prepareStatement("SELECT * FROM db1ab14_ex03_actor")) {
            try (ResultSet resultSet = selection.executeQuery()) {
                while (resultSet.next()) {
                    actorList.add(new DB_Actor(resultSet.getString(1), resultSet.getString(2)));
                }
            }
        }
        return actorList;
    }

    public List<DB_Film> getFilmTable(Connection connection) throws SQLException {

        try (PreparedStatement selection = connection.prepareStatement("SELECT * FROM db1ab14_ex03_film")) {
            try (ResultSet resultSet = selection.executeQuery()) {
                while (resultSet.next()) {
                    filmList.add(new DB_Film(resultSet.getString(1), resultSet.getString(2), resultSet.getString(2)));
                }
            }
        }

        return filmList;
    }

    public List<DB_Relationship_Actor_Films> getRAFTable(Connection connection) throws SQLException {

        try (PreparedStatement selection = connection.prepareStatement("SELECT * FROM db1ab14_ex03_act")) {
            try (ResultSet resultSet = selection.executeQuery()) {
                while (resultSet.next()) {
                    RAF_List.add(new DB_Relationship_Actor_Films(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
                }
            }
        }
        return RAF_List;
    }

    public List<DB_Relationship_Film_Producer> getRFPTable(Connection connection) throws SQLException {
        try (PreparedStatement selection = connection.prepareStatement("SELECT * FROM db1ab14_ex03_have")) {
            try (ResultSet resultSet = selection.executeQuery()) {
                while (resultSet.next()) {
                    RFP_List.add(new DB_Relationship_Film_Producer(resultSet.getString(1), resultSet.getString(2)));
                }
            }
        }
        return RFP_List;
    }

}
