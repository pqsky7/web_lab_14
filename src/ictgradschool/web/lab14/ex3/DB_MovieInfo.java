package ictgradschool.web.lab14.ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_MovieInfo {
    private List<DB_Actor> actorList = new ArrayList<>();
    private List<DB_Film> filmList = new ArrayList<>();
    private List<DB_Relationship_Actor_Films> RAF_List = new ArrayList<>();
    private List<DB_Relationship_Film_Producer> RFP_List = new ArrayList<>();

    public DB_MovieInfo(List<DB_Actor> actorList, List<DB_Film> filmList, List<DB_Relationship_Actor_Films> RAF_List, List<DB_Relationship_Film_Producer> RFP_List) {
        this.actorList = actorList;
        this.filmList = filmList;
        this.RAF_List = RAF_List;
        this.RFP_List = RFP_List;
    }

    public List<DB_Actor> getActorList() {
        return actorList;
    }

    public List<DB_Film> getFilmList() {
        return filmList;
    }

    public List<DB_Relationship_Actor_Films> getRAF_List() {
        return RAF_List;
    }

    public List<DB_Relationship_Film_Producer> getRFP_List() {
        return RFP_List;
    }

    public void setActorList(List<DB_Actor> actorList) {
        this.actorList = actorList;
    }

    public void setFilmList(List<DB_Film> filmList) {
        this.filmList = filmList;
    }

    public void setRAF_List(List<DB_Relationship_Actor_Films> RAF_List) {
        this.RAF_List = RAF_List;
    }

    public void setRFP_List(List<DB_Relationship_Film_Producer> RFP_List) {
        this.RFP_List = RFP_List;
    }
}
