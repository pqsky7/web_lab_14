package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_MovieInfoDAO {
    public DB_MovieInfo getMoviesInfo() {
        DB_MovieInfo MovieInfo = null;
        List<DB_Actor> actorList = new ArrayList<>();
        List<DB_Film> filmList = new ArrayList<>();
        List<DB_Relationship_Actor_Films> RAF_List = new ArrayList<>();
        List<DB_Relationship_Film_Producer> RFP_List = new ArrayList<>();
        final String[] databaseNames = {"SELECT * FROM db1ab14_ex03_actor", "SELECT * FROM db1ab14_ex03_film", "SELECT * FROM db1ab14_ex03_act", "SELECT * FROM db1ab14_ex03_have"};
        // Set the database name to your database
            /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful");
            for (String databaseName : databaseNames) {
                try (PreparedStatement selection = conn.prepareStatement(databaseName)) {
                    try (ResultSet resultSet = selection.executeQuery()) {
                        while (resultSet.next()) {
                            switch (databaseName) {
                                case "SELECT * FROM db1ab14_ex03_actor":
                                    actorList.add(new DB_Actor(resultSet.getString(1),resultSet.getString(2)));
                                    break;
                                case "SELECT * FROM db1ab14_ex03_film":
                                    filmList.add(new DB_Film(resultSet.getString(1),resultSet.getString(2),resultSet.getString(2)));
                                    break;
                                case "SELECT * FROM db1ab14_ex03_act":
                                    RAF_List.add(new DB_Relationship_Actor_Films(resultSet.getString(1),resultSet.getString(2),resultSet.getString(3),resultSet.getString(4)));
                                    break;
                                case "SELECT * FROM db1ab14_ex03_have":
                                    RFP_List.add(new DB_Relationship_Film_Producer(resultSet.getString(1),resultSet.getString(2)));
                                    break;
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    MovieInfo = new DB_MovieInfo(actorList,filmList,RAF_List,RFP_List);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return MovieInfo;
    }
}
