package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 4/05/2017.
 */
public class DB_MovieInfoDAO_Typical {
    DB_MovieInfo MovieInfo;
    List<DB_Actor> actorList;
    List<DB_Film> filmList;
    List<DB_Relationship_Actor_Films> RAF_List;
    List<DB_Relationship_Film_Producer> RFP_List;

    public DB_MovieInfoDAO_Typical() {
        MovieInfo = null;
        actorList = new ArrayList<>();
        filmList = new ArrayList<>();
        RAF_List = new ArrayList<>();
        RFP_List = new ArrayList<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DB_MovieInfo getMoviesInfo() {
        return new DB_MovieInfo(getActorTable(), getFilmTable(), getRAFTable(), getRFPTable());
    }

    public List<DB_Actor> getActorTable() {
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful, loading actor table...");
            try (Statement selection = conn.createStatement()) {
                selection.executeQuery("SELECT * FROM db1ab14_ex03_actor");
                try (ResultSet resultSet = selection.getResultSet()) {
                    while (resultSet.next()) {
                        actorList.add(new DB_Actor(resultSet.getString(1), resultSet.getString(2)));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actorList;
    }

    public List<DB_Film> getFilmTable() {
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful, loading film table...");
            try (Statement selection = conn.createStatement()) {
                selection.executeQuery("SELECT * FROM db1ab14_ex03_film");
                try (ResultSet resultSet = selection.getResultSet()) {
                    while (resultSet.next()) {
                        filmList.add(new DB_Film(resultSet.getString(1), resultSet.getString(2), resultSet.getString(2)));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filmList;
    }

    public List<DB_Relationship_Actor_Films> getRAFTable() {
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful, loading RAF table...");
            try (Statement selection = conn.createStatement()) {
                selection.executeQuery("SELECT * FROM db1ab14_ex03_act");
                try (ResultSet resultSet = selection.getResultSet()) {
                    while (resultSet.next()) {
                        RAF_List.add(new DB_Relationship_Actor_Films(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return RAF_List;
    }

    public List<DB_Relationship_Film_Producer> getRFPTable() {
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/qpen546", Config.getProperties())) {
            System.out.println("Connection successful, loading RFP table...");
            try (Statement selection = conn.createStatement()) {
                selection.executeQuery("SELECT * FROM db1ab14_ex03_have");
                try (ResultSet resultSet = selection.getResultSet()) {
                    while (resultSet.next()) {
                        RFP_List.add(new DB_Relationship_Film_Producer(resultSet.getString(1), resultSet.getString(2)));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return RFP_List;
    }

}
